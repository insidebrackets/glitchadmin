import { createStore } from 'vuex'
import { abi, contractAddr } from '@/scripts/abi'
import axios from "axios";
import w3 from '@/scripts/w3' 
import constants from '@/scripts/const' 
//import createPersistedState from "vuex-persistedstate";
const baseUrl = "http://127.0.0.1:8000/";
const version = "v1/"
const contract = w3.polyContract(abi,contractAddr )

export default createStore({
  /*
  plugins: [createPersistedState({
        storage: window.sessionStorage,
    })],
    */
  state: {
    addr:null,
    products:[],
    tx:[],
    notify:[],
    bundelAddr: null
  },
  getters:{
    bundelAddr(state){
      return state.bundelAddr
    },
    notify(state){
      return state.notify[0]
    },
    address(state){
      return state.addr
    },
    getbundelByAddr: (state) => (addr) =>{
      return state.products.find(p => p.creator === addr )
    },
    products(state){
      //return state.products.filter( p => p.open )
       return state.products
    },
    buyProducts(state){
      //return state.products.filter( p => p.open )
       return state.products.filter(p => !p.open )
    },
    tx(state){
       return state.tx.filter(t => !t.verified )
    },
    isAuthenticated(state){
      return !!state.addr
    }
  },
  mutations: {
   addProducts(state, payload){
     state.products.push(payload)
   },
   addNote(state, payload){
      console.log(payload)
      state.notify = [...payload] 
   },
   addTx(state, payload){
     state.tx = [...payload]
   },
   addProductDetial(state,payload){
     state.products.splice(payload.index,0,payload.obj)
   },
   removeProductDetial(state,payload){ 
    state.products.splice(payload,1);
   },
   clearProducts(state){
    state.products = []; 
   },
   addAddr(state, payload){
    state.addr = payload;
   }, 
   addAccouts(state, payload){
    state.accouts.push[payload];
   }, 
    removeAddr(state){
      state.addr = null;
    }
  },
  actions: {
    remove(context){  
      const ProductDetailInddex = context.getters.products.findIndex(x => x.kind === "ProductDetail")
      context.commit('removeProductDetial', ProductDetailInddex )
    },
    update(context, payload){
      //console.log(payload)
      console.log("Upade ++++++++++++++++++++++++++++++")
      console.log( "index " + payload.index)
      console.log( "id " + payload.obj.id)
      //const clickedProductInddex = context.getters.products.findIndex(x => && x.id === payload.index )
      //console.log(clickedProductInddex)
      if(context.getters.products.length <= payload.index){
        context.commit('addProducts', payload.obj  )
      }else{
        console.log("push")
        context.commit('addProductDetial',{ index: payload.index , obj: payload.obj }  )
        //this.products.splice( index,0, objDetail)
      }
    },
    async updateTxSc({ dispatch },payload){
       await contract.methods.updateTx(payload.id, payload.tx).call({
               from: constants.ADDR,
       },function(err, result){
          if(err){ console.log(err) }
          if(result){ 
            console.log(result) 

            localStorage.removeItem('txObj');
            console.log("ge saved")
          }  
       })     
       
    },
    async load(context){
      if(window.ethereum){
        console.log("store +++++++++++++++++++++++++++++++++++++")
        context.commit('clearProducts')
        console.log(abi)        
        console.log(contract)
        //const events = await contract.getPastEvents("Launch",  { fromBlock: "latest"});
        // get the index of mapping
        const crowdIndex = await contract.methods.count().call()
        console.log(crowdIndex)
        // loop tru all mamping elements and store it into vuex 
        for(var i =1; i <= crowdIndex; i++ ){ 
           await contract.methods.fetchData(i).call({
                   from:  constants.ADDR,
           },function(err, result){
              if(err){ console.log(err) }
              if(result){ 
                console.log(result) 
                let addr_list = result.addr_list.filter(x =>!x.includes("0x0000000000000000000000000000000000000000") )
                context.commit('addProducts', { id: i , ...result, kind: 'Product', addr_list: addr_list })
              }  
           })
        } 
      }
    },
    async accountlist(context){

      const accounts = await window.ethereum.request({ method: "eth_accounts" })
      console.log(accouts)
      //context.commit('addAccouts', payload)
    },
    send(context,payload){
      context.commit('addAddr', payload)
    },
    async updateDB(context, payload){

        const contract = w3.polyContract(abi,contractAddr);
        const index = await contract.methods.count().call();

        var bundels;
      
         await contract.methods.fetchData(index).call({
                 from: constants.ADDR,
         },function(err, result){
            if(err){ console.log(err) }
            if(result){ 
              //console.log(result) 
              let addr_list = result.addr_list.filter(x =>!x.includes("0x0000000000000000000000000000000000000000") )
              bundels = { id: index , ...result, tname: 'bundels', addr_list: addr_list }
            }
        
         })

        //const keys = Object.keys(bundels).filter(key => !isNaN(parseFloat(key)) && !isNaN(key - 0)) 

        let bundel =  Object.fromEntries( Object.entries(bundels).filter(([key, value]) => !!isNaN(parseFloat(key)) && !!isNaN(key - 0)) )
        
        // turn bundels obj to string and in crypt it with sha3      
        const text = JSON.stringify(bundel)
        const secret = w3.prov.utils.sha3(text) 
        console.log(text.length)
        // sign a the message 
        const promiseSig = w3.prov.eth.personal.sign( secret , myAddr )
        const sign = await promiseSig

        console.log(sign)
        // create a obj with the id the message and signed message
        /*
        const obj = {
          id : index,
          //text: text,
          bundel: { ...bundel, sign: sign },
          //message: secret,
          sign_message : sign 
        }
        */
        console.log(bundel)
 
        // send the 
        const resp = await axios({
          method: 'post',
          url: baseUrl + 'bundel',
          data: { ...bundel , sign: sign},
          headers: { 'Content-Type': 'application/json' }
        })
        
    
    },
    async loadTxDB({commit}){
      const resp = await axios({
        method: 'get',
        url: baseUrl + version + 'tx',
        headers: { 'Content-Type': 'application/json' }
      })
      console.log("loadTxDB")
      let tx_box = [];
      for(let i=0; i < resp.data.length; i++){
        tx_box.push(resp.data[i])
      } 
      commit('addTx', tx_box )
    },
    async addSmart({commit,dispatch}, payload){
      const resp = await axios({
        method: 'patch',
        url: baseUrl + 'update',
        data: payload,
        headers: { 'Content-Type': 'application/json' }
      })

      console.log(resp)
      if(resp.statusText === "OK"){
        dispatch('loadTxDB')
      }
        
    },
    async login({getters}){
      const resp = await axios({
        method: 'post',
        url: baseUrl + version + 'login',
        data:{ address: getters.address },
        headers: { 'Content-Type': 'application/json' }

      })
      console.log(resp)
    },
    logout(context){
      context.commit('removeAddr')
    }
  },
  modules: {
  }
})
