import Web3 from 'web3'
import { OpenSeaPort, Network } from 'opensea-js'
import axios from "axios";
import constants from '@/scripts/const'
import w3 from '@/scripts/w3'
// This example provider won't let you make transactions, only read-only calls:
//let resp = await axios.get(polygonscanUrl)
console.log(Network.Rinkeby)
const seaport = new OpenSeaPort( w3.ether_infura, {
  networkName: Network.Main,
  apiKey: constants.OPENSEA_KEY })

console.log(seaport)



//console.log(seaport.api.getAsset())
export default {
   async balance(){
    //const nft = await seaport.api.getAsset({ tokenAddress:'0xd00e79629e2053d837285c74a0ec09f51b33c141' , tokenId:2676} )
     //console.log(nft)

   },
  async loadAssset(addr,id){
    const url = `https://api.opensea.io/api/v1/asset/${ addr }/${ id }` 
    let resp = await axios.get(url, {    
      headers: { 'X-API-KEY': constants.OPENSEA_KEY }
    })
    console.log(resp)

  }
}

