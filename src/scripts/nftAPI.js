import axios from "axios";
import constants from '@/scripts/const'

//let resp = await axios.get(polygonscanUrl)

export default {
  async getAsset(addr, id){

  var nft, respR, respO, raribleUrl,openSeaUrl, platform = [];

  try {
     raribleUrl = `https://api-staging.rarible.org/v0.1/items/ETHEREUM:${addr}:${id}` 
      respR = await axios.get(raribleUrl)    
      nft = { 
      price: respR.data.bestBidOrder.takePrice,
      image: respR.data.meta.content[0].url
      }
    platform.push("rarible")
  } catch (err) {    
    console.log("not on rarible") 
  } 

  try {
     openSeaUrl = `https://api.opensea.io/api/v1/asset/${addr}/${id}`
    let respO = await axios.get(openSeaUrl, {    
      headers: { 'X-API-KEY': constants.OPENSEA_KEY }
    })
    console.log(respO)
    nft = {  
      price: respO.data.orders ? respO.data.orders[0].current_price : "0.0",
      image: respO.data.image_original_url 
    }
    platform.push("opensea")
  } catch (err) {
    console.log("not on opensea") 
  } 
    nft["platform"] = platform 
    //console.log(respO)
    return nft
  } 

    
}
