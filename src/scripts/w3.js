import Web3 from 'web3'
import constants from '@/scripts/const'
const w3 = new Web3(window.ethereum);
const prov = w3;
const ether = new Web3(constants.ETH_ALCHEMY_API);
const poly = new Web3(constants.POLY_ALCHEMY_API);
const ether_infura = new Web3.providers.HttpProvider(constants.ETH_INFURA_API)

export default {
  prov,
  poly,
  ether,
  ether_infura,
  rinkContract(abi, addr){
    const contract = new ether.eth.Contract(abi,addr);
    return contract;
  },
  polyContract(abi, addr){
    const contract = new poly.eth.Contract(abi,addr);
    return contract;
  },
  contract(abi, addr){
    const contract = new w3.eth.Contract(abi,addr);
    return contract;
  },
  async gasPrice(){
    let gasPrice; 
   return gasPrice = await w3.eth.getGasPrice();
  },
  async gasLimit(){
    let gasLimit;
    const block = await w3.eth.getBlock("latest");
    return gasLimit = parseInt(block.gasLimit/block.transactions.length);
  },
  async payback(addr,addrList){
    const result = function(err,result){
      if(error){
        console(errior)
      }
      if(result){
        console(reuslt)
      }
    }
    for (let i = 0; i < addresses.length; i++) {
      await w3.prov.eth.sendTransaction({from:addr, to:addrList[i], value:web3.toWei(amount_in_ether,"ether")}, result); 
    } 
  }
}


