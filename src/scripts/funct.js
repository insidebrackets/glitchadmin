import w3 from '@/scripts/w3'
export default {
  fourAvatar(list){
    return list.filter(( avatar  ,idx) => idx < 4)
  },
 colorAddr(address){
    let str = address.toString() 
    var hash = 0;
    for (var i = 0; i < str.length; i++) {
        hash = str.charCodeAt(i) + ((hash << 5) - hash);
    }
    var colour = '#';
    for (var i = 0; i < 3; i++) {
        var value = (hash >> (i * 8)) & 0xFF;
        colour += ('00' + value.toString(16)).substr(-2);
    }
    return colour;
 },
  kether(number){
    const ether = w3.prov.utils.fromWei( number, 'ether')
    return parseFloat(ether).toFixed(3);
  },
  sellPrice(quantity,goal){
    const nftPrice = (Number(quantity) * Number(goal)) * 2
    const ether = w3.prov.utils.fromWei( nftPrice.toString(), 'ether')
    return parseFloat(ether).toFixed(3);
  },
  nftPrice(quantity, goal){
    const nftPrice = (Number(quantity) * Number(goal) )
    const ether = w3.prov.utils.fromWei( nftPrice.toString(), 'ether')
    return parseFloat(ether).toFixed(3);
  },
  addrShorter(addr,num){ 
    const n = (num === undefined) ? 4 : num 
    const start = addr.substring(0,n+2).toLowerCase() 
    const end = addr.substring((42-n),42).toLowerCase() 
    return `${start}...${end}`
  },
  async switchChain(chainId){
      try {
        await window.ethereum.request({
        method: 'wallet_switchEthereumChain',
          params: [{ chainId: w3.prov.utils.toHex(chainId) }],
        });
    } catch (error) {
      console.error(error);
    } 
  },
  transactionsbyAddr(addr){
    var currentBlock = w3.ether.eth.blockNumber;
    var n = w3.ether.eth.getTransactionCount(addr, currentBlock);
    var bal = w3.ether.eth.getBalance(addr, currentBlock);
    for (var i=currentBlock; i >= 0 && (n > 0 || bal > 0); --i) {
        try {
            var block = w3.ether.eth.getBlock(i, true);
            if (block && block.transactions) {
                block.transactions.forEach(function(e) {
                    if (addr == e.from) {
                        if (e.from != e.to)
                            bal = bal.plus(e.value);
                        console.log(i, e.from, e.to, e.value.toString(10));
                        --n;
                    }
                    if (addr == e.to) {
                        if (e.from != e.to)
                            bal = bal.minus(e.value);
                        console.log(i, e.from, e.to, e.value.toString(10));
                    }
                });
            }
        } catch (e) { console.error("Error in block " + i, e); }
    } 
  }
}


