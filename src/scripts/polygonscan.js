import axios from "axios";
import constants from '@/scripts/const'

const polygonscanKey = constants.POLYGON_SCAN_KEY 
const polygonscanMainApi = "https://api.polygonscan.com"
const polygonscanTestAPi = "https://api-testnet.polygonscan.com"
export const contractAddr = "0x1F0f14F871D6F193880A188319223Ce6025eaCAF"

export default {
  getContractAddr(){
    return contractAddr
  },
  async loadAbi(networkId){
    let polygonscanUrl = "" 
    // Number because networkid is a string nummer "80001"
    if( Number(networkId) === 80001){
      polygonscanUrl = `${ polygonscanTestAPi }/api?module=contract&action=getabi&address=${ contractAddr }&apikey=${ polygonscanKey}`
    }else if( Number(networkId) === 137){
      polygonscanUrl = `${ polygonscanTestAPi }/api?module=contract&action=getabi&address=${ contractAddr }&apikey=${ polygonscanKey}`
    }else{
      return false;
    }

    try {
      let resp = await axios.get(polygonscanUrl)
      console.log("hey loadAbi") 
      console.log()
      if(resp.data.message === "OK"){
        return resp.data.result 
      }
    } catch (e) {
      console.log(e)
      return false;
    }
  }
}
