import Web3 from 'web3'
import axios from "axios";

const web3 = new Web3(window.ethereum) 
const env = "rinkeby"

import { createRaribleSdk, RaribleSdk } from "@rarible/protocol-ethereum-sdk"
import { Web3Ethereum } from "@rarible/web3-ethereum"
const raribleSdk = createRaribleSdk(new Web3Ethereum({ web3 }), env )

console.log(raribleSdk)

export default {
    async buy(obj){
      console.log(obj) 
      const raribleUrl = `https://ethereum-api-staging.rarible.org/v0.1/order/orders/sell/byItemAndByStatus?contract=${obj.image_addr}&tokenId=${obj.image_id}&status=ACTIVE` 
      const respR = await axios.get(raribleUrl)    
      console.log(respR.data)
      if(respR.data.orders.length>=1){
       const order = respR.data.orders[0];
       let response = await raribleSdk.order.buy({ order,originFees: 0, amount: 1, infinite: true })
       return response.hash
      }
      /*
      const contractErc20Address: Address = '0x0' // your ERC20 contract address
      const contractErc721Address: Address = '0x0' // your ERC721 contract address
      const tokenId: BigNumber = '0x0' // the ERC721 Id of the token on which we want to place a bid
      const sellerAddress: Address = '0x0' // Owner of ERC721 token
      const buyerAddress: Address = '0x0' // Who make a bid
      const nftAmount: number = 1 // For ERC721 always be 1
      const bidPrice: number = 10 // price per unit of ERC721 or ERC1155 token(s)
      const request = {
        makeAssetType: {
          assetClass: "ERC20",
          contract: contractErc20Address,
        },
        maker: buyerAddress,
        takeAssetType: {
          assetClass: "ERC721",
          contract: contractErc721Address,
          tokenId: tokenId,
        },
        taker: sellerAddress,
        amount: nftAmount,
        originFees: [],
        payouts: [],
        price: bidPrice,
      }

    */
    }
}
