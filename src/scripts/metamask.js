import store from '@/store'

export default {

  async setup(networkId){
        let that = this;

        // https://ethereum.stackexchange.com/questions/93502/metamask-api-cant-detect-events-connect-and-disconnect-in-react-js 
        // https://ethereum.stackexchange.com/questions/42768/how-can-i-detect-change-in-account-in-metamask
        //  check if user hass been locked out from metamask
        window.ethereum.on("accountsChanged", async function(accounts) {
           
          if(accounts.length === 0){
            console.log("have been logout") 
            store.dispatch("logout") 
            window.localStorage.removeItem('abi');
          } 

        });

        // check if user habe been changed the metamask network
        window.ethereum.on('chainChanged', function (networkId) {
          console.log("network have been changed")
        })

         const accounts  = await window.ethereum.request({ method: "eth_accounts" })
         //console.log(accounts)

         // checks the length of the accounts if there is no accouts that means the 
         // user is not loged in
        
        if(accounts.length !== 0 ){
         const accounts  = await window.ethereum.request({
           method: "eth_accounts"
          })
          //console.log(store)
          store.dispatch("send", accounts[0]) 
          that.$forceUpdate;
        }
  }
}
